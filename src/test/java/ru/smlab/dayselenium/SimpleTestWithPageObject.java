package ru.smlab.dayselenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.smlab.dayselenium.pages.ArticlePage;
import ru.smlab.dayselenium.pages.HomePage;

public class SimpleTestWithPageObject {

    HomePage homePage = new HomePage();
    ArticlePage articlePage = new ArticlePage();

    @Test
    public void testWiki1() {
        String userQuery = "Java";
        homePage.openPage();
        homePage.enterSearchQuery(userQuery);
        homePage.clickSearchButton();
        String actualTitle = articlePage.getArticleTitle();
    }

    @Test(description = "тот же тест, но с использованием паттерна Chain of Invocations")
    public void testWiki2() {
        String userQuery = "Java";

        homePage.openPage();
        String actualTitle =
                homePage
                .enterSearchQuery(userQuery)
                .clickSearchButton()
                .getArticleTitle();

        Assert.assertEquals(actualTitle, userQuery, "Article header doesn't match!");
    }

}
