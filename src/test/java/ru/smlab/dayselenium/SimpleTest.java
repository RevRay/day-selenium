package ru.smlab.dayselenium;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SimpleTest {

    static WebDriver driver;

    static final String searchInputXpath = "//input[@id='searchInput']";
    static final String searchButtonXpath = "//button[@type='submit']";
    static final String articleHeaderXpath = "//h1[@id=\"firstHeading\"]";

    static final String URL = "https://www.wikipedia.org/";

    @BeforeClass
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "bin/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @Test
    public void testWiki() {
        String userSearchQuery = "Java";

        driver.get(URL);

        WebElement searchInput = driver.findElement(By.xpath(searchInputXpath));
        searchInput.sendKeys(userSearchQuery);

        driver.findElement(By.xpath(searchButtonXpath)).click();

        WebElement articleHeader = driver.findElement(By.xpath(articleHeaderXpath));

        Assert.assertEquals(articleHeader.getText(), userSearchQuery, "Article header doesn't match!");
    }

    @Test
    public void testWikiPython() {
        String userSearchQuery = "Python";

        driver.get(URL);

        WebElement searchInput = driver.findElement(By.xpath(searchInputXpath));
        searchInput.sendKeys(userSearchQuery);
        driver.findElement(By.xpath(searchButtonXpath)).click();

        WebElement articleHeader = driver.findElement(By.xpath(articleHeaderXpath));

        Assert.assertEquals(articleHeader.getText(), userSearchQuery, "Article header doesn't match!");
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }

}
