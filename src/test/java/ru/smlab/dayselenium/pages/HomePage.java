package ru.smlab.dayselenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class HomePage extends BasePage{

    static final String searchInputXpath = "//input[@id='searchInput']";
    static final String searchButtonXpath = "//button[@type='submit']";

    public HomePage enterSearchQuery(String query) {
        WebElement searchInput = driver.findElement(By.xpath(searchInputXpath));
        searchInput.sendKeys(query);
        return this;
    }

    public HomePage scrollToFooterSection() {
        //TODO
        return this;
    }

    public ArticlePage clickSearchButton() {
        driver.findElement(By.xpath(searchButtonXpath)).click();
        return new ArticlePage();
    }

    @Override
    public void openPage() {
        driver.get(URL);
    }
}
