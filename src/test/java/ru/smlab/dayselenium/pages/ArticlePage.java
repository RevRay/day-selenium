package ru.smlab.dayselenium.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ArticlePage extends BasePage{

    static final String articleHeaderXpath = "//h1[@id=\"firstHeading\"]";

    @Override
    public void openPage() {
        driver.get(URL + "/articles");
    }

    public String getArticleTitle() {
        return driver.findElement(By.xpath(articleHeaderXpath)).getText();
    }
}
