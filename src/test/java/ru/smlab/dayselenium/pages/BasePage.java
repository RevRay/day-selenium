package ru.smlab.dayselenium.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public abstract class BasePage {

    static final String URL = "https://www.wikipedia.org/";

    static WebDriver driver;

    static {
        System.setProperty("webdriver.chrome.driver", "bin/chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    abstract public void openPage();

}
